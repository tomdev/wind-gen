package com.zmr.wind.gen.util;

import java.io.File;

import com.youbenzi.mdtool.tool.MDTool;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.ReUtil;

public class MdUtil
{
    // markdown目录
    private static String mdPath  = "C:\\Users\\Administrator\\Downloads\\ruoyi\\md";

    // 输出路径
    private static String outPath = "C:\\Users\\Administrator\\Desktop";

    /**
     * 将md文件夹下的markdown文件转换成html并输出到指定目录
     * 配置mdPath和outPath
     * @param args
     * @author zmr
     */
    public static void main(String[] args)
    {
        String docPath = new ClassPathResource("doc").getAbsolutePath();
        FileUtil.copy(docPath, outPath, false);
        File[] mds = FileUtil.ls(mdPath);
        StringBuffer mdbf = new StringBuffer();
        for (File file : mds)
        {
            mdbf.append(FileUtil.readUtf8String(file) + "\n");
        }
        String mdcontent = mdbf.toString();
        String mdhtml = MDTool.markdown2Html(mdcontent);
        String html = FileUtil.readUtf8String(docPath + File.separator + "index.html");
        String doc = ReUtil.replaceAll(html, "(##content##)", mdhtml);
        FileWriter writer = new FileWriter(outPath + File.separator + "doc" + File.separator + "index.html");
        writer.write(doc);
    }
}
